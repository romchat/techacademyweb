declare interface ITechnicalAttentionBulletinWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'TechnicalAttentionBulletinWebPartStrings' {
  const strings: ITechnicalAttentionBulletinWebPartStrings;
  export = strings;
}
