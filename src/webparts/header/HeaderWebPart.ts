import "jquery";
import "bootstrap";
import "popper.js";

import * as strings from 'HeaderWebPartStrings';

import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';

import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { Version } from '@microsoft/sp-core-library';
import { escape } from '@microsoft/sp-lodash-subset';
import styles from './HeaderWebPart.module.scss';

require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');
require('../../../node_modules/fontawesome-free-5.10.2-web/css/all.min.css');

export interface IHeaderWebPartProps {
  description: string;
}

export default class HeaderWebPart extends BaseClientSideWebPart<IHeaderWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
    <div class="${styles.header}">
      <div class="contain-fluid" style="box-shadow: 0 4px 8px 0 rgba(0,0,0,.2); border:1px solid black;">
          <div style="text-align:center; padding-top: 10px; background-color: ghostwhite;">                
              <a href="../page/index.html">
                  <img class="${styles.pttlogo}" src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/Edt7x9eY9B1Dg7LGEU0N0VQBoZtvsWnHtA6QGOIHx2q3Fg?e=CgcMxT" alt="">
              </a>
          </div>
          <nav class="${styles.navbar}">
              <div class="${styles.navcollapse}" id="myTopnav">
                  <li class="${styles.navitem}"><a class="${styles.navlink} active" href="../page/CareerPath.html">Career Path</a></li>
                  <li class="${styles.navitem}"><a class="${styles.navlink}" href="#">Tab</a></li>
                  <li class="${styles.navitem}"><a class="${styles.navlink}" href="#">Course Training</a></li>
                  <li class="${styles.navitem}"><a class="${styles.navlink}" href="../page/SuggestionDiscipline.html">Suggestion Discipline</a></li>
                  <li class="${styles.navitem}"><a class="${styles.navlink}" href="../page/SpecialistInformation.html">Specialist Informmation</a></li>
                  <li class="${styles.navitem}"><a class="${styles.navlink}" href="#">Technical Authority</a></li>
                  <li class="${styles.navitemright}"><a class="${styles.navlink}" href="#">MasterList of Conference</a></li>
              </div>
          </nav>
      </div>
    </div>`;

  var header = document.getElementById("myTopnav");
  var btns = header.getElementsByClassName(`$(styles.${styles.navlink})`);
  for (var i = 0; i < btns.length; i++){
	  btns[i].addEventListener("click", function(){
		var current = document.getElementsByClassName("active");
		current[0].className = current[0].className.replace(" active", "");
		this.className += " active";
	  });
  }
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
