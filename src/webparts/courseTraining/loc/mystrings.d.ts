declare interface ICourseTrainingWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'CourseTrainingWebPartStrings' {
  const strings: ICourseTrainingWebPartStrings;
  export = strings;
}
