import "jquery";
import "datatables.net";
import "bootstrap";
import "popper.js";
import "datatables.net-select";
import "bootstrap-datepicker";
import "datatables.net-responsive";
import "datatables.net-rowreorder";

import * as strings from 'CourseTrainingWebPartStrings';

import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';

import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { Version } from '@microsoft/sp-core-library';
import { escape } from '@microsoft/sp-lodash-subset';
import styles from './CourseTrainingWebPart.module.scss';

require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');
require('../../../node_modules/fontawesome-free-5.10.2-web/css/all.min.css');
require('../../../node_modules/datatables.net/css/jquery.dataTables.min.css');

export interface ICourseTrainingWebPartProps {
  description: string;
}

export default class CourseTrainingWebPart extends BaseClientSideWebPart<ICourseTrainingWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.courseTraining }">
        <div class="container-fluid">
          <div class="${styles.searchbar} row">
            <div class="col"><div class="input-group">
                <select class="custom-select" id="inputGroupSelect01">
                  <option selected disabled>Discipline</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
              </div>
            </div>
            <div class="col"><div class="input-group">
                <select class="custom-select" id="inputGroupSelect01">
                  <option selected disabled>Competency</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
              </div>
            </div>
            <div class="col"><div class="input-group">
                <select class="custom-select" id="inputGroupSelect01">
                  <option selected disabled>Month/Year</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
              </div>
            </div>
            <input type="text" id="mySearchText" placeholder="Search...">
            <button id="mySearchButton">Search</button>
          </div>
            <table id="course" class="display nowrap compact" style="width:100%">
              <thead>
                <tr style="background-color:rgb(214, 230, 244)">
                  <th>No.</th>
                  <th>วันที่อบรม</th>
                  <th>ชื่อหลักสูตร</th>
                  <th>ชื่อคนสอน</th>
                  <th>ระยะเวลา</th>
                  <th>Discipline</th>
                  <th>Competency</th>
                  <th>สถานที่</th>
                  <th>Detail</th>
                  <th>สำรองที่นั่ง</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>No.1</th>
                  <th>วันที่อบรม1</th>
                  <th>ชื่อหลักสูตร1</th>
                  <th>ชื่อคนสอน1</th>
                  <th>ระยะเวลา1</th>
                  <th>Discipline1</th>
                  <th>Competency1</th>
                  <th>สถานที่1</th>
                  <th>Detail1</th>
                  <th>สำรองที่นั่ง1</th>
                </tr>
                <tr>
                  <th>No.2</th>
                  <th>วันที่อบรม2</th>
                  <th>ชื่อหลักสูตร2</th>
                  <th>ชื่อคนสอน2</th>
                  <th>ระยะเวลา2</th>
                  <th>Discipline2</th>
                  <th>Competency2</th>
                  <th>สถานที่2</th>
                  <th>Detail2</th>
                  <th>สำรองที่นั่ง2</th>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>`;

      $(document).ready(() => {
        var table = $('#course').DataTable({
          "dom": '<"top"i>rt<"bottom"><"clear">',
          rowReorder: {
            selector: 'td:nth-child(2)'
          },
          responsive: true
        });

        $('#mySearchButton').on( 'keyup click', () => {
          console.log((<HTMLInputElement>document.getElementById('mySearchText')).value);
          table.search((<HTMLInputElement>document.getElementById('mySearchText')).value).draw();
        });
      });
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
