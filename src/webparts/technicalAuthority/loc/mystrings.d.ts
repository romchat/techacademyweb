declare interface ITechnicalAuthorityWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'TechnicalAuthorityWebPartStrings' {
  const strings: ITechnicalAuthorityWebPartStrings;
  export = strings;
}
