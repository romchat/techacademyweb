import "jquery";
import "datatables.net";
import "bootstrap";
import "popper.js";
import "datatables.net-select";
import "bootstrap-datepicker";
import "datatables.net-responsive";
import "datatables.net-rowreorder";
import "datatable-buttons";
import "buttons-flash";
import "jszip";
import "pdfmake";
import "vfs_fonts";
import "buttons-html5";
import "buttons-print";

import { Version } from '@microsoft/sp-core-library';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { escape } from '@microsoft/sp-lodash-subset';
import {SPComponentLoader} from '@microsoft/sp-loader';

import styles from './TechnicalAuthorityWebPart.module.scss';
import * as strings from 'TechnicalAuthorityWebPartStrings';

require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');
require('../../../node_modules/fontawesome-free-5.10.2-web/css/all.min.css');
require('../../../node_modules/datatables.net/css/jquery.dataTables.min.css');

export interface ITechnicalAuthorityWebPartProps {
  description: string;
}

export default class TechnicalAuthorityWebPart extends BaseClientSideWebPart<ITechnicalAuthorityWebPartProps> {
  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.technicalAuthority }">
        <div class="container-fluid">
          <div class="${styles.searchbar} row">
          <input type="text" id="mySearchText" placeholder="ID+Name">
            <div class="col"><div class="input-group">
                <select class="custom-select" id="inputGroupSelect01">
                  <option selected disabled>Job Family</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
              </div>
            </div>
            <div class="col"><div class="input-group">
                <select class="custom-select" id="inputGroupSelect01">
                  <option selected disabled>Technical Authority</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
              </div>
            </div>
            <input type="text" id="mySearchText" placeholder="Plant">
            <button id="mySearchButton">Search</button>
          </div>
            <table id="course" class="display nowrap compact" style="width:100%">
              <thead>
                <tr style="background-color:rgb(214, 230, 244)">
                  <th>ID</th>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Job Family</th>
                  <th>Plant</th>
                  <th>Dept</th>
                  <th>Indicator</th>
                  <th>Technical Authority</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Position</th>
                  <th>
                    <select class="custom-select" id="inputGroupSelect01">
                      <option selected disabled>Test1</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </th>
                  <th>Plant</th>
                  <th>Dept</th>
                  <th>Indicator</th>
                  <th>
                    <select class="custom-select" id="inputGroupSelect01">
                      <option selected disabled>test1</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </th>
                </tr>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Position</th>
                  <th>
                    <select class="custom-select" id="inputGroupSelect01">
                      <option selected disabled>Test2</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </th>
                  <th>Plant</th>
                  <th>Dept</th>
                  <th>Indicator</th>
                  <th>
                    <select class="custom-select" id="inputGroupSelect01">
                      <option selected disabled>test2</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>`;

      $(document).ready(() => {
        var table = $('#course').DataTable({
          "dom": 'Bfrtip',
          //buttons: ['pdf','excel','copy','csv'],
          rowReorder: {
            selector: 'td:nth-child(2)'
          },
          responsive: true
        });

        $('#mySearchButton').on( 'keyup click', () => {
          console.log((<HTMLInputElement>document.getElementById('mySearchText')).value);
          table.search((<HTMLInputElement>document.getElementById('mySearchText')).value).draw();
        });
      });
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
