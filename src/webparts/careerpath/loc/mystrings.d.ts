declare interface ICareerpathWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'CareerpathWebPartStrings' {
  const strings: ICareerpathWebPartStrings;
  export = strings;
}
