import "jquery";
import "datatables.net";
import "bootstrap";
import "popper.js";
import "datatables.net-select";
import "bootstrap-datepicker";

import * as strings from 'CareerpathWebPartStrings';

import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';

import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { Version } from '@microsoft/sp-core-library';
import { escape } from '@microsoft/sp-lodash-subset';
import styles from './CareerpathWebPart.module.scss';

require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');
require('../../../node_modules/fontawesome-free-5.10.2-web/css/all.min.css');
require('../../../node_modules/datatables.net/css/jquery.dataTables.min.css');

export interface ICareerpathWebPartProps {
  description: string;
}

export default class CareerpathWebPart extends BaseClientSideWebPart<ICareerpathWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
    <div class="${styles.careerpath}">
      <div class="container-fluid">
          <div class="${styles.barmenu}">
              <div class="${styles.dropdown}">
                  <button class="${styles.dropbtn}">Technical Progression Scheme (TPS)
                  <i class="fa fa-caret-down"></i>
                  </button>
                  <div class="${styles.dropdowncontent}">
                  <button class="btn" type="button" id="Instrument1">Instrument</button>
                  <button class="btn" type="button" id="Mechanical1">Mechanical</button>
                  <button class="btn" type="button" id="Electrical1">Electrical</button>
                  </div>
              </div>
              <div class="${styles.dropdown}">
                  <button class="${styles.dropbtn}">Engineering (EPS)
                  <i class="fa fa-caret-down"></i>
                  </button>
                  <div class="${styles.dropdowncontent}">
                  <button class="btn" type="button" id="Instrument2">Instrument</button>
                  <button class="btn" type="button" id="Mechanical2">Mechanical</button>
                  <button class="btn" type="button" id="Electrical2">Electrical</button>
                  </div>
              </div>
              <div class="${styles.dropdown}">
                  <button class="${styles.dropbtn}">Chemical Eng (CEPS)
                  <i class="fa fa-caret-down"></i>
                  </button>
                  <div class="${styles.dropdowncontent}">
                  <button class="btn" type="button" id="Process Engineer">Process Engineer</button>
                  <button class="btn" type="button" id="Production Engineer">Production Engineer</button>
                  <button class="btn" type="button" id="Process Control">Process Control</button>
                  </div>
              </div>
          </div>
          <table id="example" class="display nowrap compact" style="width:100%; padding-top: 2px;">
              <thead>
                <tr style="background-color:#c6cfff; color:#616161;">
                    <th></th>
                    <th></th>
                    <th style="text-align:center" id="main">Instrument</th>
                    <th></th>
                    <th></th>
                </tr>
                <tr style="background-color: #deecff; color:#616161; text-align: center">
                    <th></th>
                    <th style="width: 10px"></th>
                    <th>competency list</th>
                    <th></th>
                    <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                    <th>F0237</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>Plant engineering knowledge</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star"></span>
                    </th>
                </tr>
                <tr>
                    <th>F0238</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>Operation Excellence Management System(OEMS)</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    </th>
                </tr>
                <tr>
                    <th>F0270</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>Site safety practices</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    </th>
                </tr>
                <tr>
                    <th>F0271</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>Process safety practices</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star"></span>
                    </th>
                </tr>
                <tr>
                    <th>F0272</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>Environmental protection management</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    </th>
                </tr>
                <tr>
                    <th>F0252</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>Engineering economics</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    </th>
                </tr>
                <tr>
                    <th>F0253</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>Routine maintenance program(PdM, PM)</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star"></span>
                    </th>
                </tr>
                <tr>
                    <th>F0254</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>Routine maintenance planning and scheduling</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star "></span>
                        <span class="fa fa-star "></span>
                        <span class="fa fa-star "></span>
                        <span class="fa fa-star "></span>
                    </th>
                </tr>
                <tr>
                    <th>F0255</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>Routine maintenance execution and supervision</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    </th>
                </tr>
                <tr>
                    <th>F0256</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>maintenance support</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star"></span>
                    </th>
                </tr>
                <tr>
                    <th>F0257</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>Turnaround planning and scheduling</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    </th>
                </tr>
                <tr>
                    <th>F0258</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>Turnaround execution method and supervision</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    </th>
                </tr>
                <tr>
                    <th>F0259</th>
                    <th><button type="button" class="btn"><img src="https://betteritcorp.sharepoint.com/:i:/s/test/techacademyweb2/EcKvhngNH7VJnaF3vr59fuYBLyPfX_iJzXkKNVPJ_wZlkQ?e=gj67z7" style="height: 25px; width: auto"></button></th>
                    <th>Contract & contractor performance</th>
                    <th>xx/xx/xxxx</th>
                    <th>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star ${styles.checked}"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    </th>
                </tr>
              <tbody>
          </table>
      </div>
    </div>`;

      $(document).ready(() => {
        $('#example').DataTable();
        document.getElementById('Instrument1').addEventListener('click', ()=> {
          document.getElementById('main').innerHTML = 'Instrument';
        });
        document.getElementById('Mechanical1').addEventListener('click', ()=> {
          document.getElementById('main').innerHTML = 'Mechanical';
        });
        document.getElementById('Electrical1').addEventListener('click', ()=> {
          document.getElementById('main').innerHTML = 'Electrical';
        });
        document.getElementById('Instrument2').addEventListener('click', ()=> {
          document.getElementById('main').innerHTML = 'Instrument';
        });
        document.getElementById('Mechanical2').addEventListener('click', ()=> {
          document.getElementById('main').innerHTML = 'Mechanical';
        });
        document.getElementById('Electrical2').addEventListener('click', ()=> {
          document.getElementById('main').innerHTML = 'Electrical';
        });
        document.getElementById('Process Engineer').addEventListener('click', ()=> {
          document.getElementById('main').innerHTML = 'Process Engineer';
        });
        document.getElementById('Production Engineer').addEventListener('click', ()=> {
          document.getElementById('main').innerHTML = 'Production Engineer';
        });
        document.getElementById('Process Control').addEventListener('click', ()=> {
          document.getElementById('main').innerHTML = 'Process Control';
        });
      });

      $.noConflict();
    }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
