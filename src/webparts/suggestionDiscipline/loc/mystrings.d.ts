declare interface ISuggestionDisciplineWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'SuggestionDisciplineWebPartStrings' {
  const strings: ISuggestionDisciplineWebPartStrings;
  export = strings;
}
