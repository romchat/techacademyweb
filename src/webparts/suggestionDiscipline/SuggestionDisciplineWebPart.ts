import "jquery";
import "datatables.net";
import "bootstrap";
import "popper.js";
import "datatables.net-select";
import "bootstrap-datepicker";
import "datatables.net-responsive";
import "datatables.net-rowreorder";
import "paginationjs";

import * as strings from 'SuggestionDisciplineWebPartStrings';

import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { escape, fromPairs } from '@microsoft/sp-lodash-subset';

import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { Version } from '@microsoft/sp-core-library';
import styles from './SuggestionDisciplineWebPart.module.scss';

require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');
require('../../../node_modules/fontawesome-free-5.10.2-web/css/all.min.css');
require('../../../node_modules/datatables.net/css/jquery.dataTables.min.css');
require('../../../node_modules/paginationjs/dist/pagination.css');

export interface ISuggestionDisciplineWebPartProps {
  description: string;
}

export default class SuggestionDisciplineWebPart extends BaseClientSideWebPart<ISuggestionDisciplineWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.suggestionDiscipline }">
      <div class="container-fluid">
        <div class="row">
            <div class="${styles.sidebar}">
                <div class="${styles.sidebarheader}">
                <a>Discipline</a>
                </div>
                <div class="${styles.sidebarbody}" id="sidebody">
                <a class="${styles.sideitem}">Instrument</a>
                <a class="${styles.sideitem}">Mechanical</a>
                <a class="${styles.sideitem}">Eletrical</a>
                <a class="${styles.sideitem}">Process Engineer</a>
                <a class="${styles.sideitem}">Production Engineer</a>
                <a class="${styles.sideitem}">Process Control</a>
                </div>
            </div>
            <div class="${styles.contentcontain}">
            <div class="row" style="margin:0; padding:5px;">
                <div class="row" style="margin:0; position:absolute; right:10px;">
                <input type="text" id="mySearchText" placeholder="Search...">
                <button id="mySearchButton"><i class="fa fa-search"></i></button>
                </div>
                <div style="display: flex; align-items: center;">
                <a>Show</a>
                    <select class="custom-select" id="entitynum">
                    <option selected value="5">5</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    </select>
                <a>entries</a>
                </div>
            </div>
            <div id="data-container"></div>
            <div id="pagination-container" style="float: right; padding: 5px;"></div>
            </div>
          </div>
        </div>
      </div>`;

      $(document).ready( () => {
        var ent = 5;
        document.getElementById("entitynum").addEventListener('change',() => {
            ent = (<any>document.getElementById("entitynum")).value;
            (<any>$('#pagination-container')).pagination({
                dataSource: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                pageSize: ent,
                showNavigator: true,
                callback: (data, pagination) => {
                    // template method of yourself
                    var html = template(data, pagination);
                    $('#data-container').html(html);
                }
            });

            function template(data, pagination) {
                var html = '<ul style="display: flex; flex-direction: column">';
                $.each(data, (index, item) => { 
                    html += '<div class="row" style="background-color: #ffffffbf; width:50%; margin-bottom: 10px; border-radius: 5px;"><div class="col-12"><a style="font-size:20px"><i class="far fa-file-pdf"></i> NameDoc#</a></div>'+'<div class="col-12"><a style="font-size:18px">TypeDoc</a><a style="font-size:18px; margin-left:20px">Last modifying</a></div></div>';
                });
                var total = pagination.pageNumber*pagination.pageSize;
                html += '</ul><a>'+total+'/'+pagination.totalNumber+'</a>';
                return html;
            }
          });
      });
    }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
