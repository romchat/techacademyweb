import "jquery";
import "datatables.net";
import "bootstrap";
import "popper.js";
import "datatables.net-select";
import "bootstrap-datepicker";
import "datatables.net-responsive";
import "datatables.net-rowreorder";

import { Version } from '@microsoft/sp-core-library';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './MasterListofConferenceWebPart.module.scss';
import * as strings from 'MasterListofConferenceWebPartStrings';

require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');
require('../../../node_modules/fontawesome-free-5.10.2-web/css/all.min.css');
require('../../../node_modules/datatables.net/css/jquery.dataTables.min.css');

export interface IMasterListofConferenceWebPartProps {
  description: string;
}

export default class MasterListofConferenceWebPart extends BaseClientSideWebPart<IMasterListofConferenceWebPartProps> {

    public render(): void {
      this.domElement.innerHTML = `
        <div class="${ styles.masterListofConference }">
          <div class="container-fluid">
            <div class="${styles.searchbar} row">
              <input class="col-3" type="text" id="mySearchText" placeholder="Place" style="padding:5px; margin:5px">
              <input class="col-3" type="text" id="mySearchText" placeholder="Conference" style="padding:5px; margin:5px">
              <input class="col-3" type="text" id="mySearchText" placeholder="Organize By" style="padding:5px; margin:5px">
              <button id="mySearchButton" style="padding:5px; margin:5px">Search</button>
            </div>
              <table id="course" class="display nowrap compact" style="width:100%; border-spacing:0">
                <thead style="background-color:rgb(214, 230, 244)">
                  <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Conference</th>
                    <th rowspan="2">Org Level</th>
                    <th rowspan="2">Organize By</th>
                    <th rowspan="2">Place</th>
                    <th colspan="3" style="text-align:center">Paticiplant Level</th>
                    <th rowspan="2">Interval</th>
                    <th rowspan="2">Detail</th>
                  </tr>
                  <tr>
                    <th>Participant*</th>
                    <th>Speaker</th>
                    <th>Sponsorship**</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>1</th>
                    <th>conference</th>
                    <th>
                      <select class="custom-select" id="inputGroupSelect01">
                        <option selected disabled>Test1</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                    </th>
                    <th>organize</th>
                    <th>place</th>
                    <th>Dept</th>
                    <th>Indicator</th>
                    <th>test</th>
                    <th>Interval</th>
                    <th><button type="button" class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i></button></th>
                  </tr>
                </tbody>
              </table>
            </div>
        
          <!-- Modal -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Detail</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul id="myTab" class="nav ${styles.navtabs}" role="tablist">
                                <li role="presentation"><a class="${styles.active}" href="#Tab1" aria-controls="Tab1" role="tab" data-toggle="tab">Organized</a></li>
                                <li role="presentation"><a href="#Tab2" aria-controls="Tab2" role="tab" data-toggle="tab">Tab2</a></li>
                                <li role="presentation"><a href="#Tab3" aria-controls="Tab3" role="tab" data-toggle="tab">Tab3</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="Tab1">
                                  <table id="tab1" class="dispaly nowrap" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th colspan="6" style="text-align: center;">Organized Event(Year)</th>
                                      </tr>
                                      <tr>
                                        <th>2016</th>
                                        <th>2017</th>
                                        <th>2018</th>
                                        <th>2019</th>
                                        <th>2020</th>
                                        <th>2021</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="Tab2">
                                  <table id="tab2" class="dispaly nowrap" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th>Expection</th>
                                        <th>Target Group</th>
                                        <th>Minimum Seat</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="Tab3">
                                  <table id="tab3" class="dispaly nowrap" style="width:100%">
                                    <thead>
                                      <tr>
                                        <th>Channal Point</th>
                                        <th>Member/Registration Fees</th>
                                        <th>Reference Information</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary save">Save changes</button>
                    </div>
                </div>
            </div>
          </div>
        </div>`;
  
        $(document).ready(() => {

          $('#tab1').DataTable({"searching":false,responsive: true,rowReorder: {selector: 'td:nth-child(2)'}});
          $('#tab2').DataTable({"searching":false,responsive: true,rowReorder: {selector: 'td:nth-child(2)'}});
          $('#tab3').DataTable({"searching":false,responsive: true,rowReorder: {selector: 'td:nth-child(2)'}});

          var table = $('#course').DataTable({
            "dom": '<"top"i>rt<"bottom"><"clear">',
            rowReorder: {
              selector: 'td:nth-child(2)'
            },
            responsive: true
          });
          var flag = true;
          
          $(`#myTab li a`).click(function(event){
            $(this).tab('show');
            if(flag == true){ //remove first tab when click first time
            $(`#myTab li a:first`).removeClass(`${styles.active}`);
            flag = false;
            }
          });

          $(`#myTab li a`).on('show.bs.tab', (event) => {
            var x = $(event.target);         // active tab
            var y = $(event.relatedTarget);  // previous tab
            x.addClass(`${styles.active}`);
            y.removeClass(`${styles.active}`);
          });
  
          $('#mySearchButton').on( 'keyup click', () => {
            console.log((<HTMLInputElement>document.getElementById('mySearchText')).value);
            table.search((<HTMLInputElement>document.getElementById('mySearchText')).value).draw();
          });
        });
    }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
