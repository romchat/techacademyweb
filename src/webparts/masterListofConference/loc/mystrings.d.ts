declare interface IMasterListofConferenceWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'MasterListofConferenceWebPartStrings' {
  const strings: IMasterListofConferenceWebPartStrings;
  export = strings;
}
