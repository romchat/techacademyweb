declare interface ISpecialistInformationWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'SpecialistInformationWebPartStrings' {
  const strings: ISpecialistInformationWebPartStrings;
  export = strings;
}
