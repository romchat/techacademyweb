import "jquery";
import "datatables.net";
import "bootstrap";
import "popper.js";
import "datatables.net-select";
import "bootstrap-datepicker";
import "bootstrap-slider";
import "datatables.net-responsive";
import "datatables.net-rowreorder";

import * as strings from 'SpecialistInformationWebPartStrings';

import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { ItemAddResult, default as pnp } from "sp-pnp-js";

import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { PnPClientStorage } from "sp-pnp-js";
import { Version } from '@microsoft/sp-core-library';
import { escape } from '@microsoft/sp-lodash-subset';
import styles from './SpecialistInformationWebPart.module.scss';

require('../../../node_modules/bootstrap/dist/css/bootstrap.min.css');
require('../../../node_modules/fontawesome-free-5.10.2-web/css/all.min.css');
require('../../../node_modules/datatables.net/css/jquery.dataTables.min.css');

export interface ISpecialistInformationWebPartProps {
  description: string;
}

export default class SpecialistInformationWebPart extends BaseClientSideWebPart<ISpecialistInformationWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.specialistInformation }">
        <div class="container-fluid">

          <!--Main Page-->
          <fieldset id="main_set">
            <div id="filter" class="row" style="border-bottom:1px solid black; margin-bottom: 10px; padding: 10px">
              <button class="col-2 btn btn-primary" id="addbtn" data-toggle="modal" data-target=".bd-example-modal-lg" type="button"><i class="fas fa-plus"></i> Add</button>
              <div class="col-10">
                <select class="custom-select" id="inputGroupSelect01">
                  <option selected disabled>division</option>
                  <option) value="1">division1</option>
                  <option) value="2">division2</option>
                  <option) value="3">division3</option>
                </select>
              </div>
            </div>
            <div id="show_section" class="row col-12" style="text-align:center">
            </div>  
          </fieldset>

          <!--Detail Page-->
          <fieldset id="detail_set" style="display:none"> 
          </fieldset>

          <!-- Add Modal -->
          <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Add Personnel Profile</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form class="row">
                    <h5 class="col-2">Name: </h5><input id="in_name" class="${styles.input} col-4" placeholder="Name. . ." type="text"></input>
                    <h5 class="col-2">Title: </h5><input id="in_title" class="${styles.input} col-4" placeholder="Position. . ." type="text"></input>
                    <h5 class="col-2">TelePhone: </h5><input id="in_tele" maxlength="10" class="${styles.input} col-4" placeholder="Telephone. . ." type="text"></input>
                    <h5 class="col-2">Mobile: </h5><input id="in_mob" maxlength="10" class="${styles.input} col-4" placeholder="Mobile. . ." type="text"></input>
                    <h5 class="col-2">E-mail: </h5><input id="in_email" class="${styles.input} col-4" placeholder="Email. . ." type="email"></input>
                    <h5 class="col-3">WorkLocation: </h5><input id="in_wl" class="${styles.input} col-3" placeholder="Worklocation. . ." type="text"></input>
                  </form>
                  <form>
                    <table id="exp_table" class="display" style="width:100%">
                      <thead>
                        <tr>
                          <th>Expertise</th>
                          <th>Percentage of expertise(%)</th>
                          <th><button type="button" class="btn btn-secondary ${styles.tooltip}" id="addrow"><span class="${styles.tooltiptext}">Add Row</span><i class="fas fa-plus-circle"></i></button></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </form>
                  <form>
                    <h5 class="col-2">Introduce:</h5><input id="in_intro" class="col-10" placeholder="Introduce. . ." type="text"></input>
                    <h5 class="col-2">Education:</h5><input id="in_edu" class="col-10" placeholder="Education. . ." type="text"></input>
                    <h5 class="col-2">WorkHistory:</h5><input id="in_wh" class="col-10" placeholder="Work History. . ." type="text"></input>
                    <h5 class="col-2">Experience:</h5><input id="in_exp" class="col-10" placeholder="Experience. . ." type="text"></input>
                    <h5 class="col-2">Certifications:</h5><input id="in_cert" class="col-10" placeholder="Certifications. . ." type="text"></input>
                  </form>
                  <form>
                    <h5 class="col-2">Picture:</h5><input class="col-10" type="file" id="picture" placeholder="Select Picture. . ." required></input>
                  </form>
                </div>
                <div class="modal-footer">
                  <input type="submit" class="btn btn-primary" id="savebtn" value="Add"></input>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancle</button>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>`;

      $(document).ready(()=>{
        var save = document.getElementById("savebtn");
        save.addEventListener('click',savedata);
        let count = 1;

        var t = $('#exp_table').DataTable({"paging": false, "ordering": false, "dom": '<>', "info": false});
        
        $('#addrow').on('click', () => {
          t.row.add(['<tr><td><input id="row-'+count+'-exname" type="text"></input></td>','<td><input id="row-'+count+'-exval" type="number" min="0" max="100"></input></td>','<td></td></tr>']).draw(false);
          count++;
        });

        $('#addrow').click();
        
        let c = this;
        c.renderdata();

        let file;
        $(document).on('change','#picture',(e) => {
          file = e.target.files[0];
        });

        function savedata(){
          if($('in_name').val() !== undefined){
            console.log("In Savedata");
            
          var exp = "";
          for(let i=0; i<count-1; i++){
            exp += t.cells(i,0).nodes().to$().find('input').val() +","+ t.cells(i,1).nodes().to$().find('input').val()+",";
          }
          

          // let imgName = this.state.allCoverImage.length+'_'+file.name;
          
          let imgName = file.name;
          
          pnp.sp.web.getFolderByServerRelativeUrl(`PublishingImages/Employee`).files.add(imgName,file,true).then(()=>{
            c.addfunc(exp,imgName);
    
            // pnp.sp.web.lists.getByTitle(this.props.listName).items.add({
            //   Title: `${inputTitle.value}`,
            //   WIDescription:`${inputDes.value}`,
            //   DocumentLibrary: `${inputFolder.value}`,
            //   CoverImage: ${file}
            // }).then(()=>{
            //   this.setState({
            //     createStep: 1,
            //     stepText: 'Create '
            //   });
            //   this.toggleResultModal();
            //   this.getAllCoverImage();
            //   this.getDepartmentList();
            // });
          });
        }else{
          alert("Enter Name");
        }
      }
      });
  }

  private renderdata(){
    pnp.sp.web.lists.getByTitle("profile").items.get().then((items: any[]) => {
      var show = document.getElementById("show_section");
      
      for(let i=0; i<items.length; i++){
        show.innerHTML += `<div class="col-3 ${styles.profile_item} " data-name="${items[i].ID}">
                            <div class="${styles.profile_photo_wrapper}">
                              <object class="${styles.image}" data="https://betteritcorp.sharepoint.com/sites/test/techacademyweb2/PublishingImages/Employee/`+items[i].Picture+`" type="image/png">
                                <img class="${styles.image}" src="https://betteritcorp.sharepoint.com/sites/test/techacademyweb2/PublishingImages/default-profile.png"/>
                              </object>
                            </div>
                            <div class="${styles.profile_item_main}">
                              <a class="${styles.name}">`+items[i].name+`</a>
                              <a class="${styles.position}">`+items[i].Title+`</a>
                              <a class="${styles.position}">`+items[i].Introduce+`</a>
                            </div>
                          </div>`;
      }
    }).then(() => {
      $(document).on('click',`.${styles.profile_item}`,function(){
        nextfunc($(this).data("name"));
      });

      function nextfunc(test) {
        pnp.sp.web.lists.getByTitle("profile").items.filter("ID eq '"+test+"'").get().then((items: any[]) => {
          var detail = document.getElementById("detail_set");
          
          detail.innerHTML = `<div style="padding-top: 0.1rem; padding-bottom: 0.1rem;">
          <div class="row" style="font-size:25px; padding: 10px;">
            <i class="fas fa-arrow-left" id="backbtn"></i>
            <button class="btn btn-primary" id="edit" style="position: absolute; right: 20px; top: 5px;" data-toggle="modal" data-target="#editModal"><i class="fas fa-edit"></i> Edit</button>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="col-12">
              <object class="${styles.image}" data="https://betteritcorp.sharepoint.com/sites/test/techacademyweb2/PublishingImages/Employee/`+items[0].Picture+`" style="margin: 5px" type="image/png">
                <img class="${styles.image}" src="https://betteritcorp.sharepoint.com/sites/test/techacademyweb2/PublishingImages/default-profile.png"/>
              </object>
                  <a style="margin:5px; font-size: 25px;" class="${styles.name}" id="name">`+items[0].name+`</a>
                  <a style="margin:5px; color:#286eae; font-size: 20px; font-weight: bold;" class="${styles.position}" id="position">`+items[0].Title+`</a>
              </div>
              <div class="col-12" style="margin:5px; margin-top: 20px">
                  <div class="row" style="background-color: #cadff4; margin:0;">
                    <a style="margin:5px; font-size:22px; font-weight:bold; color:black;";>CONTACT INFORMATION</a>
                  </div>
                  <div class="col-12" id="info-left" style="background-color: #f5deb333">
                  <div class="row"><a class="col-4" style="margin:5px; font-weight:bold; font-size:20px">Telephone:</a><textarea class="col-6" style="margin:5px" disabled>`+items[0].Telephone+`</textarea></div>
                  <div class="row"><a class="col-4" style="margin:5px; font-weight:bold; font-size:20px">Mobile:</a><textarea class="col-6" style="margin:5px" disabled>`+items[0].Mobile+`</textarea></div>
                  <div class="row"><a class="col-4" style="margin:5px; font-weight:bold; font-size:20px">Email:</a><textarea class="col-6" style="margin:5px" disabled>`+items[0].Email+`</textarea></div>
                  <div class="row"><a class="col-4" style="margin:5px; font-weight:bold; font-size:20px">Work Location:</a><textarea class="col-6" style="margin:5px" disabled>`+items[0].Worklocation+`</textarea></div>
              </div>
            </div>
            <div class="col-12" style="padding-bottom: 20px; padding-top:5px;">
              <div class="row" style="background-color: #cadff4; margin:0;">
                <a style="margin:5px; font-weight:bold; font-size:22px; color:black;">EXPERTISE</a>
              </div>
              <div style="background-color: #f5deb333; padding-bottom: 10px" id="expertise_section">
              </div>
            </div>
              </div>
              <div class="col-6" id="info-right">
                <div class="row">
                    <a style="margin:5px; font-weight:bold; color:black; font-size:22px; background-color: #cadff4; width:100%">INTRODUCE</a>
                    <textarea style="margin:5px; width:100%" disabled>`+items[0].Introduce+`</textarea>
                </div>
                <div class="row">
                    <a style="margin:5px; font-weight:bold; color:black; font-size:22px; background-color: #cadff4; width:100%">EDUCATIONAL</a>
                    <textarea style="margin:5px; width:100%" disabled>`+items[0].Education+`</textarea>
                </div>
                <div class="row">
                    <a style="margin:5px; font-weight:bold; color:black; font-size:22px; background-color: #cadff4; width:100%">WORK HISTORY</a>
                    <textarea style="margin:5px; width:100%" disabled>`+items[0].Workhistory+`</textarea>
                </div>
                <div class="row">
                    <a style="margin:5px; font-weight:bold; color:black; font-size:22px; background-color: #cadff4; width:100%">EXPERIENCE</a>
                    <textarea style="margin:5px; width:100%" disabled>`+items[0].Experience+`</textarea>
                </div>
                <div class="row">
                    <a style="margin:5px; font-weight:bold; color:black; font-size:22px; background-color: #cadff4; width:100%">CERTIFICATIONS</a>
                    <textarea style="margin:5px; width:100%" disabled>`+items[0].Certification+`</textarea>
                </div>
              </div>
            </div>
          </div>
          
          <!-- edit Modal -->
          <div class="modal fade bd-example-modal-lg" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Edit `+items[0].name+` Profile</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form class="row">
                    <h5 class="col-2">Name: </h5><input id="edit_name" class="${styles.input} col-4" value="`+items[0].name+`" type="text" required></input>
                    <h5 class="col-2">Title: </h5><input id="edit_title" class="${styles.input} col-4" value="`+items[0].Title+`" type="text" required></input>
                    <h5 class="col-2">TelePhone: </h5><input id="edit_tele" class="${styles.input} col-4" maxlength="10" value="`+items[0].Telephone+`" type="text"></input>
                    <h5 class="col-2">Mobile: </h5><input id="edit_mob" class="${styles.input} col-4" maxlength="10" value="`+items[0].Mobile+`" type="text"></input>
                    <h5 class="col-2">E-mail: </h5><input id="edit_email" class="${styles.input} col-4" value="`+items[0].Email+`" type="email"></input>
                    <h5 class="col-3">WorkLocation: </h5><input id="edit_wl" class="${styles.input} col-3" value="`+items[0].Worklocation+`" type="text"></input>
                  </form>
                  <form>
                    <table id="edit_table" class="display" style="width:100%">
                      <thead>
                        <tr>
                          <th>Expertise</th>
                          <th>Percentage of expertise(%)</th>
                          <th><button type="button" class="btn btn-secondary ${styles.tooltip}" id="addrow"><span class="${styles.tooltiptext}">Add Row</span><i class="fas fa-plus-circle"></i></button></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </form>
                  <form>
                    <h5 class="col-2">Introduce:</h5><input id="edit_intro" value="`+items[0].Introduce+`" class="col-10" type="text"></input>
                    <h5 class="col-2">Education:</h5><input id="edit_edu" value="`+items[0].Education+`" class="col-10" type="text"></input>
                    <h5 class="col-2">WorkHistory:</h5><input id="edit_wh" value="`+items[0].Workhistory+`" class="col-10" type="text"></input>
                    <h5 class="col-2">Experience:</h5><input id="edit_exp" value="`+items[0].Experience+`" class="col-10" type="text"></input>
                    <h5 class="col-2">Certifications:</h5><input id="edit_cert" value="`+items[0].Certification+`" class="col-10" type="text"></input>
                  </form>
                  <form>
                    <h5 class="col-2">Picture:</h5><input class="col-10" type="file" id="picture"></input>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary" data-dismiss="modal" id="save_edit">Save changes</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancle</button>
                </div>
              </div>
            </div>
          </div>`;

          let data = items[0].Expertise;
          let ex = data.split(",");
          let exper_sec = document.getElementById("expertise_section");
          var t = $('#edit_table').DataTable({"paging": false, "ordering": false, "dom": '<>', "info": false});  
          let table_data1 = "";   
          let table_data2 = "";  
          let table_data3 = "";    
          let count = 1;
          for(let i=0;i<ex.length-1;i++){
            if(i%2 == 0){
              exper_sec.innerHTML += `<div class="col-12" style="margin-bottom:10px">
                                      <a style="margin:5px; font-weight:bold; font-size:16px">`+ex[i]+`</a>`;
              table_data1 = `<tr><td><input id="row-`+count+`-exname" value="`+ex[i]+`" type="text"></input></td>`;
            }else if(i%2 != 0){
              exper_sec.innerHTML += `<div class="progress">
                                            <div class="progress-bar" style="width: `+ex[i]+`%" role="progressbar" aria-valuenow="`+ex[i]+`" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </div>`;
              table_data2 = `<td><input id="row-`+count+`-exval" type="number" value="`+ex[i]+`" min="0" max="100"></input></td>`;
              table_data3 = `<td></td></tr>`;
              t.row.add([table_data1,table_data2,table_data3]).draw(false);
              count++;
            }else{}
          }

          var edit = document.getElementById('save_edit');
          edit.addEventListener('click',updatefunc);

          $('#addrow').on('click', () => {
            t.row.add(['<tr><td><input id="row-'+count+'-exname" type="text"></input></td>','<td><input id="row-'+count+'-exval" type="number" min="0" max="100"></input></td>','<td></td></tr>']).draw(false);
            count++;
          });

          let file;
          $(document).on('change','#picture',(e) => {
            file = e.target.files[0];
          });
          
          function updatefunc(){
            var exp = "";
            for(let i=0; i<count-1; i++){
              exp += t.cells(i,0).nodes().to$().find('input').val() +","+ t.cells(i,1).nodes().to$().find('input').val()+",";
            }

            let imgName = "";
            if(file != undefined){
              imgName = file.name;
            }else{
              imgName = null;
            }
            
            pnp.sp.web.getFolderByServerRelativeUrl(`PublishingImages/Employee`).files.add(imgName,file,true).then(()=>{
              let list = pnp.sp.web.lists.getByTitle("profile");
              if(imgName != null){
                list.items.getById(test).update({
                  Title: (<any>document.getElementById("edit_title")).value,
                  name: (<any>document.getElementById("edit_name")).value,
                  Telephone: (<any>document.getElementById("edit_tele")).value,
                  Mobile: (<any>document.getElementById("edit_mob")).value,
                  Email: (<any>document.getElementById("edit_email")).value,
                  Worklocation: (<any>document.getElementById("edit_wl")).value,
                  Introduce: (<any>document.getElementById("edit_intro")).value,
                  Education: (<any>document.getElementById("edit_edu")).value,
                  Workhistory: (<any>document.getElementById("edit_wh")).value,
                  Experience: (<any>document.getElementById("edit_exp")).value,
                  Certification: (<any>document.getElementById("edit_cert")).value,
                  Expertise: exp,
                  Picture: imgName,
                }).then((iar: ItemAddResult) => {
                  window.parent.location.reload();
                });
              }else{
                list.items.getById(test).update({
                  Title: (<any>document.getElementById("edit_title")).value,
                  name: (<any>document.getElementById("edit_name")).value,
                  Telephone: (<any>document.getElementById("edit_tele")).value,
                  Mobile: (<any>document.getElementById("edit_mob")).value,
                  Email: (<any>document.getElementById("edit_email")).value,
                  Worklocation: (<any>document.getElementById("edit_wl")).value,
                  Introduce: (<any>document.getElementById("edit_intro")).value,
                  Education: (<any>document.getElementById("edit_edu")).value,
                  Workhistory: (<any>document.getElementById("edit_wh")).value,
                  Experience: (<any>document.getElementById("edit_exp")).value,
                  Certification: (<any>document.getElementById("edit_cert")).value,
                  Expertise: exp,
                }).then((iar: ItemAddResult) => {
                  window.parent.location.reload();
                });
              }
            });
          }
        }).then(() => {
        var main = document.getElementById('main_set');
        main.style.display = "none";
        var detail = document.getElementById('detail_set');
        detail.style.display = "block";
        
        //change display between fieldset
        var back = document.getElementById('backbtn');
        back.addEventListener('click' ,backfunc);

          function backfunc() {
            main.style.display = "block";
            detail.style.display = "none";
          }
        });
      }
    }
    );}

  private addfunc(exp,img){
    pnp.sp.web.lists.getByTitle("profile").items.add({
      Title: (<any>document.getElementById("in_title")).value,
      name: (<any>document.getElementById("in_name")).value,
      Telephone: (<any>document.getElementById("in_tele")).value,
      Mobile: (<any>document.getElementById("in_mob")).value,
      Email: (<any>document.getElementById("in_email")).value,
      Worklocation: (<any>document.getElementById("in_wl")).value,
      Introduce: (<any>document.getElementById("in_intro")).value,
      Education: (<any>document.getElementById("in_edu")).value,
      Workhistory: (<any>document.getElementById("in_wh")).value,
      Experience: (<any>document.getElementById("in_exp")).value,
      Certification: (<any>document.getElementById("in_cert")).value,
      Expertise: exp,
      Picture: img,
    }).then((iar: ItemAddResult) => {
      window.parent.location.reload();
    });
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
